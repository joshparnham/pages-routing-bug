# pages-routing-bug

Exists to illustrate a GitLab pages bug where the presence of a folder with the same name as an `index.html` file in the same directory breaks navigation to that `index.html` file directly, contravening this note in the [documentation](https://docs.gitlab.com/ee/user/project/pages/introduction.html#resolving-ambiguous-urls):

> Note: When public/data/index.html exists, it takes priority over the public/data.html file for both the /data and /data/ URL paths.
